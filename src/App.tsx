import { Navigate, Route, Routes } from "react-router-dom";
import NavBar from "./components/NavBar";
import Home from "./pages/Home";
import InstrumentDetails from "./pages/InstrumentDetails";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <>
      <NavBar />
      <ToastContainer />
      <main className="mx-6 py-6">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/instrument/:instName" element={<InstrumentDetails />} />
          <Route path="/*" element={<Navigate to="/" />} />
        </Routes>
      </main>
    </>
  );
}

export default App;
