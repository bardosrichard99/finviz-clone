export interface HierarchyNode {
  name: string;
  children?: HierarchyNode[];
  value?: number;
  change?: number;
  description?: string;
  instType?: "stock" | "forex";
}
