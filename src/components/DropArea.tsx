import React, { ReactNode } from "react";
import { useDrop } from "react-dnd";

interface DropAreaProps {
  children?: ReactNode;
}

export const DropArea: React.FC<DropAreaProps> = ({ children }) => {
  const [{ isOver, isOverCurrent, canDrop }, drop] = useDrop(
    () => ({
      accept: "inst",
      drop(_item: unknown, monitor) {
        const didDrop = monitor.didDrop();
        if (didDrop) {
          return;
        }
      },
      collect: (monitor) => ({
        isOver: monitor.isOver(),
        isOverCurrent: monitor.isOver({ shallow: true }),
        canDrop: monitor.canDrop(),
      }),
    }),
    []
  );

  return (
    <div ref={drop} className="h-full w-full">
      {children}
    </div>
  );
};
