import React from "react";
import { useDrag } from "react-dnd";

interface CardProps {
  text: string;
  openModal: () => void;
}

export const InstrumentListItem: React.FC<CardProps> = ({
  text,
  openModal,
}) => {
  const [{ isDragging }, dragRef] = useDrag(
    () => ({
      type: "inst",
      item: { text },
      end: (item, monitor) => {
        const dropResult = monitor.getDropResult<{ text: string }>();
        if (item && dropResult) {
          openModal();
        }
      },
      collect: (monitor) => ({
        isDragging: monitor.isDragging(),
        handlerId: monitor.getHandlerId(),
      }),
    }),
    [text]
  );

  return (
    <div
      ref={dragRef}
      className={`cursor-move w-fit px-2 ${
        isDragging ? "opacity-40" : "opacity-100"
      } hover:text-blue-500`}
    >
      {text}
    </div>
  );
};
