import React, { useEffect, useRef, memo, MutableRefObject } from "react";

interface TradingViewWidgetProps {
  instName: string;
}

const TradingViewWidget: React.FC<TradingViewWidgetProps> = ({ instName }) => {
  const container: MutableRefObject<HTMLDivElement | null> = useRef(null);

  useEffect(() => {
    if (container.current && container.current.childElementCount === 0) {
      const script = document.createElement("script");
      script.src =
        "https://s3.tradingview.com/external-embedding/embed-widget-advanced-chart.js";
      script.type = "text/javascript";
      script.async = true;
      script.innerHTML = `
        {
          "autosize": true,
          "symbol": "${instName}",
          "interval": "D",
          "timezone": "Etc/UTC",
          "theme": "light",
          "style": "1",
          "locale": "en",
          "allow_symbol_change": true,
          "calendar": false,
          "support_host": "https://www.tradingview.com"
        }`;
      container.current.appendChild(script);
    }
  }, [instName]);

  return (
    <div
      className="tradingview-widget-container"
      ref={container}
      style={{ height: "100%", width: "100%" }}
    />
  );
};

export default memo(TradingViewWidget);
