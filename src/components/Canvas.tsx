import React, { useEffect, useRef } from "react";
import * as d3 from "d3";
import { HierarchyNode } from "../types";
import { useNavigate } from "react-router-dom";

//https://observablehq.com/@d3/treemap/2?intent=fork

const Canvas: React.FC<HierarchyNode> = (data) => {
  const navigate = useNavigate();
  const svgRef = useRef<SVGSVGElement>(null);
  const containerRef = useRef<HTMLDivElement>(null);

  const onItemClick = (event: any) => {
    navigate(`/instrument/${event.srcElement.__data__.data.name}`);
  };

  useEffect(() => {
    if (!svgRef.current || !containerRef.current) return;

    const container = containerRef.current;
    const svg = d3.select(svgRef.current);

    const resize = () => {
      const width = container.clientWidth;
      const height = container.clientHeight;

      svg.attr("width", width).attr("height", height);

      const root = d3
        .hierarchy<HierarchyNode>(data as any)
        .sum((d) => d.value as number)
        .sort((a, b) => (b.value as number) - (a.value as number));

      const treemapLayout = d3
        .treemap<HierarchyNode>()
        .size([width, height])
        .paddingInner(2);

      treemapLayout(root as any);

      const nodes = svg
        .selectAll<SVGGElement, d3.HierarchyRectangularNode<HierarchyNode>>("g")
        .data(root.leaves());

      nodes
        .enter()
        .append("g")
        .merge(nodes)
        .attr("transform", (d: any) => `translate(${d.x0},${d.y0})`)
        .each(function (d: any) {
          const g = d3.select(this);

          g.selectAll("rect")
            .data([d])
            .join("rect")
            .attr("width", d.x1 - d.x0)
            .attr("height", d.y1 - d.y0)
            .attr("fill", (d) =>
              (d.data.change as number) > 0 ? "green" : "red"
            )
            .attr("opacity", 0.6)
            .on("click", (event) => onItemClick(event))
            .style("cursor", "pointer");

          g.selectAll("text.name")
            .data([d])
            .join("text")
            .attr("class", "name")
            .attr("x", 5)
            .attr("y", 20)
            .text(d.data.name)
            .attr("fill", "white");

          g.selectAll("text.change")
            .data([d])
            .join("text")
            .attr("class", "change")
            .attr("x", 5)
            .attr("y", 40)
            .text(`${d.data.change}%`)
            .attr("fill", "white");
        });

      nodes.exit().remove();
    };

    resize();
    window.addEventListener("resize", resize);

    // Clean up the event listener on component unmount
    return () => {
      window.removeEventListener("resize", resize);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  return (
    <div ref={containerRef} className="h-full w-full">
      <svg ref={svgRef} className="rounded-xl"></svg>
    </div>
  );
};

export default Canvas;
