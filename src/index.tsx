import React from "react";
import ReactDOM from "react-dom/client";
import Tailwind from "primereact/passthrough/tailwind";
import "./global.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { TouchBackend } from "react-dnd-touch-backend";
import { PrimeReactProvider } from "primereact/api";

const opts = {
  enableMouseEvents: true,
};

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <BrowserRouter>
      <PrimeReactProvider value={{ unstyled: false, pt: Tailwind }}>
        <DndProvider backend={TouchBackend} options={opts}>
          <App />
        </DndProvider>
      </PrimeReactProvider>
    </BrowserRouter>
    ,
  </React.StrictMode>
);
