import { useNavigate, useParams } from "react-router-dom";
import { FaRegArrowAltCircleLeft } from "react-icons/fa";
import TradingViewWidget from "../components/TradingViewWidget";

const InstrumentDetails = () => {
  const navigate = useNavigate();
  const instName = useParams().instName;

  return (
    <>
      <div className="flex items-center mb-4">
        <div className="flex items-center w-1/6">
          <button onClick={() => navigate(-1)} className="flex items-center">
            <FaRegArrowAltCircleLeft size={26} />
            <p className="ml-2">Back</p>
          </button>
        </div>
        <div className="w-4/6 text-center">
          <h1 className="font-bold text-2xl">{instName}</h1>
        </div>
        <div className="w-1/6 text-center">
          <p className="text-lg">230,54 (+2,97)</p>
        </div>
      </div>
      <div>
        <div className="rounded-xl shadow-md bg-white h-96 mb-4 overflow-hidden">
          <TradingViewWidget instName={instName ?? ""} />
        </div>
        <div className="rounded-xl shadow-md bg-white p-4 h-32 mb-4">
          <h3>Details</h3>
        </div>
        <div className="rounded-xl shadow-md bg-white p-4 h-32">
          <h3>Events</h3>
        </div>
      </div>
    </>
  );
};

export default InstrumentDetails;
