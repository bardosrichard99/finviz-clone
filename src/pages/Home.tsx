import { useEffect, useState } from "react";
import Canvas from "../components/Canvas";
import { HierarchyNode } from "../types";
import Modal from "react-modal";
import { IoClose } from "react-icons/io5";
import { FaTrash } from "react-icons/fa";
import { toast } from "react-toastify";
import { InstrumentListItem } from "../components/InstrumentListItem";
import { DropArea } from "../components/DropArea";
import { Accordion, AccordionTab } from "primereact/accordion";

// https://rapidapi.com/twelve-data1-twelve-data-default/api/twelve-data1/
// https://react-dnd.github.io/react-dnd/
// https://finnhub.io/docs/api/

const stockApiUrl = `https://finnhub.io/api/v1/stock/symbol?exchange=US&mic=XNAS&token=${process.env.REACT_APP_FINNHUB_API_KEY}`;
const forexApiUrl = `https://finnhub.io/api/v1/forex/symbol?exchange=fxcm&token=${process.env.REACT_APP_FINNHUB_API_KEY}`;

const emptyInstData: HierarchyNode = {
  name: "stocks",
  children: [],
};

const modalStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    width: "40%",
    borderRadius: "0.75rem",
  },
};

const Home = () => {
  const [apiStockData, setApiStockData] =
    useState<HierarchyNode>(emptyInstData);
  const [loadingStock, setLoadingStock] = useState(true);
  const [apiForexData, setApiForexData] =
    useState<HierarchyNode>(emptyInstData);
  const [loadingForex, setLoadingForex] = useState(true);

  useEffect(() => {
    const storedStocks = localStorage.getItem("stocks");

    if (storedStocks) {
      const items = JSON.parse(storedStocks);
      const itemList = items.map((item: any) => ({
        name: item.displaySymbol,
        description: item.description,
        instType: "stock",
        value: 90,
        change: 1.93,
      }));

      setApiStockData({
        name: "stocks",
        children: itemList,
      });
      setLoadingStock(false);
    } else {
      fetch(stockApiUrl)
        .then((response) => {
          if (!response.ok) throw new Error("Something went wrong");
          return response.json();
        })
        .then((json) => {
          localStorage.setItem("stocks", JSON.stringify(json));
          const itemList = json.map((item: any) => ({
            name: item.displaySymbol,
            description: item.description,
            instType: "stock",
            value: 90,
            change: 1.93,
          }));
          setApiStockData({
            name: "stocks",
            children: itemList,
          });
          setLoadingStock(false);
        })
        .catch((error) => {
          console.log(error);
          setLoadingStock(false);
        });
    }
  }, []);

  useEffect(() => {
    const storedForex = localStorage.getItem("forex");

    if (storedForex) {
      const items = JSON.parse(storedForex);
      const itemList = items.map((item: any) => ({
        name: item.displaySymbol.replace("/", ""),
        description: item.description,
        instType: "forex",
        value: 90,
        change: 1.93,
      }));

      setApiForexData({
        name: "forex",
        children: itemList,
      });
      setLoadingForex(false);
    } else {
      fetch(forexApiUrl)
        .then((response) => {
          if (!response.ok) throw new Error("Something went wrong");
          return response.json();
        })
        .then((json) => {
          localStorage.setItem("forex", JSON.stringify(json));
          const itemList = json.map((item: any) => ({
            name: item.displaySymbol.replace("/", ""),
            description: item.description,
            instType: "forex",
            value: 90,
            change: 1.93,
          }));
          setApiForexData({
            name: "forex",
            children: itemList,
          });
          setLoadingForex(false);
        })
        .catch((error) => {
          console.error(error);
          setLoadingForex(false);
        });
    }
  }, []);

  const getStoredItems = (): HierarchyNode[] => {
    return JSON.parse(localStorage.getItem("selectedInstruments") || '""');
  };

  const [portfolioList, setPortfolioList] =
    useState<HierarchyNode>(getStoredItems);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [volume, setVolume] = useState("");
  const [currentInstSelection, setCurrentInstSelection] =
    useState<HierarchyNode | null>(null);

  const onPortfolioAdd = () => {
    if (!volume || parseInt(volume) <= 0) {
      toast.error("Volume must be a positive number!", { autoClose: 3000 });
      return;
    }
    setIsModalOpen(false);
    if (!currentInstSelection) {
      return;
    }
    const newInst = { ...currentInstSelection, value: parseInt(volume) };
    setPortfolioList((prevState) => {
      let newState = [];
      if (prevState.children) {
        newState = [...prevState.children, newInst];
      } else {
        newState = [newInst];
      }
      return {
        name: "stocks",
        children: newState,
      };
    });
    setCurrentInstSelection(null);
  };

  const isAlreadySelected = (instName: string) => {
    if (portfolioList.children) {
      return portfolioList.children.map((inst) => inst.name).includes(instName);
    }
    return false;
  };

  const onModalOpen = (newInst: HierarchyNode) => {
    if (
      portfolioList.children &&
      portfolioList.children.map((inst) => inst.name).includes(newInst.name)
    ) {
      setPortfolioList(() => {
        if (portfolioList.children) {
          return {
            name: "stocks",
            children:
              portfolioList.children.length === 1
                ? null
                : [
                    ...portfolioList.children.filter(
                      (inst) => inst.name !== newInst.name
                    ),
                  ],
          };
        }
      });
    } else {
      setVolume("");
      setIsModalOpen(true);
      setCurrentInstSelection(newInst);
    }
  };

  useEffect(() => {
    if (!portfolioList) {
      return;
    }
    localStorage.setItem("selectedInstruments", JSON.stringify(portfolioList));
  }, [portfolioList]);

  return (
    <div className="flex gap-6 h-full">
      <Modal
        isOpen={isModalOpen}
        onRequestClose={() => setIsModalOpen(false)}
        style={modalStyles}
      >
        <div className="flex flex-wrap justify-between">
          <h2 className="font-semibold">Please add the volume (in USD)</h2>
          <button onClick={() => setIsModalOpen(false)}>
            <IoClose size={20} />
          </button>
        </div>
        <div className="flex flex-wrap justify-around mt-2 items-center">
          <input
            type="number"
            name="volume"
            id="volume"
            className="border p-1 w-5/6"
            value={volume}
            onChange={(e) => setVolume(e.target.value)}
          />
          <span className="w-1/6 pl-2">USD</span>
          <button
            className="w-full bg-lime-500 text-white font-medium rounded-xl py-2 mt-4"
            onClick={() => onPortfolioAdd()}
          >
            Accept
          </button>
        </div>
      </Modal>

      <div className="w-3/12 h-full	">
        <h1 className="font-bold">Filter</h1>
        <div className="rounded-xl h-full overflow-auto">
          <Accordion activeIndex={0}>
            <AccordionTab header="Selected">
              {portfolioList.children?.map((inst, i) => (
                <div className="flex justify-between" key={i}>
                  <p className="text-gray-300 cursor-not-allowed px-2">
                    {inst.name}
                  </p>
                  <FaTrash
                    color="red"
                    className="cursor-pointer"
                    onClick={() => onModalOpen(inst)}
                  />
                </div>
              ))}
            </AccordionTab>
            <AccordionTab header="Stocks (NASDAQ)">
              {loadingStock && <div>Loading...</div>}
              {apiStockData.children?.map((inst, i) => (
                <div className="flex justify-between" key={inst.name}>
                  {!isAlreadySelected(inst.name) ? (
                    <InstrumentListItem
                      text={inst.name}
                      openModal={() => onModalOpen(inst)}
                    />
                  ) : (
                    <>
                      <p className="text-gray-300 cursor-not-allowed px-2">
                        {inst.name}
                      </p>
                      <FaTrash
                        color="red"
                        className="cursor-pointer"
                        onClick={() => onModalOpen(inst)}
                      />
                    </>
                  )}
                </div>
              ))}
            </AccordionTab>
            <AccordionTab header="Forex">
              {loadingForex && <div>Loading...</div>}
              {apiForexData.children?.map((inst, i) => (
                <div className="flex justify-between" key={inst.name}>
                  {!isAlreadySelected(inst.name) ? (
                    <InstrumentListItem
                      text={inst.name}
                      openModal={() => onModalOpen(inst)}
                    />
                  ) : (
                    <>
                      <p className="text-gray-300 cursor-not-allowed px-2">
                        {inst.name}
                      </p>
                      <FaTrash
                        color="red"
                        className="cursor-pointer"
                        onClick={() => onModalOpen(inst)}
                      />
                    </>
                  )}
                </div>
              ))}
            </AccordionTab>
          </Accordion>
        </div>
      </div>
      <div className="w-9/12 h-full">
        <h1 className="font-bold">Canvas</h1>
        <DropArea>
          {portfolioList.children ? (
            <Canvas {...portfolioList} />
          ) : (
            <div className="rounded-xl shadow-md h-full bg-white justify-center flex gap-1 items-center">
              <span className="mx-full my-full">Portfolio is empty</span>
            </div>
          )}
        </DropArea>
      </div>
    </div>
  );
};

export default Home;
